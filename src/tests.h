#ifndef FORTEST_TESTS_H
#define FORTEST_TESTS_H
#define NUMBER 100

char *inputdata_calc[NUMBER];

char *expecteddata_calc[NUMBER];

char *inputdata_triangle[NUMBER];

char *expecteddata_triangle[NUMBER];

char *inputdata_integral1[NUMBER];

char *inputdata_integral2[NUMBER];

char *expecteddata_integral1[NUMBER];

char *expecteddata_integral2[NUMBER];

#endif                          //FORTEST_TESTS_H
