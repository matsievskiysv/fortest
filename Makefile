# Generic GNUMakefile
# Just a snippet to stop executing under other make(1) commands
# that won't understand these lines
ifneq (,)
This makefile requires GNU Make.
endif

PROGRAM = fortest
TARGET_MAIN = $(shell find -iname "$(PROGRAM).c" 2>/dev/null)
TARGET_OBJ = $(TARGET_MAIN:%.c=%.o)
D_SRC=src
D_BIN=bin
D_BUILD=build
LIB=bstrlib
vpath %.h $(D_SRC) $(LIB)
vpath %.c $(D_SRC) $(LIB)
vpath %.o $(D_BIN)

DEPENDS = $(shell $(CC) -I bstrlib -MM $(TARGET_MAIN) | sed 's|.*: ||g' | sed 's|\.c|\.o|g' | sed 's|\.h|\.o|g')

# compiler flags
CC = gcc
CFLAGS ?= -pedantic

#linker flags
LDFLAGS ?=
LDLIBS ?=

# valgrind flags
VGFLAGS ?= --track-origins=yes --leak-check=full
LOGFILE = make.log

# docs
PD = pandoc
PDFLAGS += -s
D_DOCS_SRC = doc/md
D_DOCS_DST = doc/html
DOCS := $(patsubst %.md, %.html, $(wildcard $(D_DOCS_SRC)/*.md))

all: builds $(PROGRAM)

dev: CFLAGS+=-Wall -Wextra -g
dev: all

$(PROGRAM): $(DEPENDS)
	$(CC) $(CFLAGS) $(wildcard $(D_BUILD)/*.o) $(LDFLAGS) -o $(D_BIN)/$(PROGRAM) $(LDLIBS)


builds:
	@mkdir -p $(D_BIN)
	@mkdir -p $(D_BUILD)

builds_docs:
	@mkdir -p $(D_DOCS_DST)
  
docs: builds_docs $(DOCS)

%html: %md
	$(PD) $(PDFLAGS) -f markdown -t html $< -o $(D_DOCS_DST)/$(basename $(notdir $@)).html


# These are the pattern matching rules. In addition to the automatic
# variables used here, the variable $* that matches whatever % stands for
# can be useful in special cases.
%.o: %.c
	$(CC) $(CFLAGS) -I $(LIB) -c $< -o $(D_BUILD)/$(shell basename $@)

run: all
	$(D_BIN)/$(PROGRAM)

valgrind: all
	valgrind $(VGFLAGS) $(D_BIN)/$(PROGRAM)

indent:
	indent $(shell find . -iname "*.[ch]")

clean:
	rm -f *~ *.o *.out *.log *.html
	rm -Rf $(D_BIN) $(D_BUILD)
	rm -Rf $(D_DOCS_DST)
	rm -f md/.*.swp
	@rm -f src/*~

.PHONY: clean depend run valgrind indent docs builds
