# Программа "Интегрирование методом трапеций"
Требуется написать код программы, производящей интегрирование функций в заданной области интегрирования. 
Интегрирование должно производиться методом трапеций с равномерной сеткой с заданным количеством разбиений сетки. Алгоритм предотавлен на различных ресурсах ([[1]](http://www.cleverstudents.ru/integral/method_of_trapezoids.html "[1]"), [[2]](http://www.simumath.net/library/book.html?code=Num_Integr_formula_trap "[2]")).

Интегрирование должно производиться для 2-х функций:
	
* $f(x) = x^{2}$
* $f(x) = 1/(e^{(x*{2})}+2)$

Основное тело программы и функции должны быть записаны в различные текстовые файлы, их взаимодействие должно быть организовано в виде модулей. Описание модулей можно найти по приведённым ссылкам:  [[1]](http://www.silverfrost.com/ftn95-help/language/xmodules.aspx "[1]"), [[2]](https://en.wikibooks.org/wiki/Fortran/Fortran_examples "[2]"), [[3]](http://www.cs.mtu.edu/~shene/COURSES/cs201/NOTES/chap06/mod-syntax.html "[3]"), [[4]](http://stackoverflow.com/questions/8412834/correct-use-of-modules-subroutines-and-functions-in-fortran "[4]")

Программа должна быть написана на языке программирования `FORTRAN95`. Материалы по лексике языка доступны по [ссылке](http://www.cheat-sheets.org/#Fortran "Cheat sheet").

### Компиляция динамических библиотек:

Функции, организованные в виде модулей, требуется скомпилировать в отдельные динамические библиотеки. Для этого можно использовать команду:

```
gfortran -shared -fPIC -o [library].so [code].f95
```

### Подключение динамических библиотек:

К основному телу программы требуется подключить динамические библиотеки. Для этого можно использовать команду:
```
gfortran -o [program_name] [code].f95 -ldl $(pwd)/[library].so
```

### Выбор функции:

Программа будет использовать функцию, находящуюся в библиотеке `[library].so`. Выбор функции для работы программы предлагается организовать в виде скрипта, представленного ниже (Требуется заменить символы `???` на соответствующие локальной структуре директорий).

---

```
#!/bin/bash

DIR=???
FUN1=???.so
FUN2=???.so
TARGET=???.so

echo 'Choose function (1/2)'
read ANS
case $ANS in
	1)
		cp $DIR/$FUN1 $TARGET;;
	2)
		cp $DIR/$FUN2 $TARGET;;
esac

cp $DIR/*mod .

```
---

Скрипт требуется сохранить в текстовом файле (стандартное расширение для данных файлов - .sh). Для запуска требуется использовать команду:
```
./[script].sh
```


### Формат входных данных программы в первом режиме работы программы:
```
[левая граница области интегрирования]
[правая граница области интегрирования]
[количество разбиений сетки]
```

### Формат выходных данных в первом режиме работы программы:
```
[численное занчение]
```
,`[численное занчение]` - действительное число, **округлённое до 5и значащих цифр после запятой**

### Пример:
```
[in]:
      0
      1
      100
[out]:
      0.5
```

Для проверки правильности работы написанной программы требуется использовать следующую команду:
```
./fortest --integral [1|2] [program_name]
```
при этом файл `fortest` должен присутствовать в папке.
